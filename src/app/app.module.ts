import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/Http";

import {AgGridModule} from "ag-grid-angular/main";

import {AppComponent} from "./app.component";


import { AppRountingModule } from "./app-rounting.module";


import { NormalComponent } from './normal-component/normal.component';
import { InfiniteComponent } from './infinite-component/infinite.component';


import { PagerService } from './services/pager.services';


import { ClickableComponent } from './shared/clickable.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRountingModule,
        HttpClientModule,
        AgGridModule.withComponents(
            [
              ClickableComponent
            ])
    ],
    declarations: [
        AppComponent,
        NormalComponent,
        InfiniteComponent ,     
        ClickableComponent 
    ],
    providers:[PagerService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
